import { useRouter } from "next/router";
import Link from "next/link";
import Head from "next/head";
import Image from "next/image";
import cls from "classnames";
import React, { useEffect, useState } from 'react';
import styles from "../../styles/coffee-store.module.css";

const Event = () => {
    const router = useRouter()
    var id = router.query.id
    const [events, setEvents] = useState([]);
    useEffect(() => {
        if (id) {
            var url="http://local.uchile.cl:3000/api/events?id="+id
            // Fetch data from the API endpoint
            fetch(url)
            .then((response) => response.json())
            .then((data) => setEvents(data))
            .catch((error) => console.error('Error fetching data:', error));
        }
    }, [id]);

    if (!events) {
        return <div>Loading...</div>;
    }
    
    return (
        <div>
        {Object.keys(events).length > 0 ? (
            <div>
                {events.map((event) => ( 
                    <div className={styles.layout}>
                    <Head>
                      <title>{event.title}</title>
                      <meta name="description" content={`${event.description} coffee store`} />
                    </Head>
                    <div className={styles.container}>
                      <div className={styles.col1}>
                        <div className={styles.backToHomeLink}>
                          <Link href="/">← Back to home</Link>
                        </div>
                        <div className={styles.nameWrapper}>
                          <h1 className={styles.name}>{event.title}</h1>
                        </div>
                        <Image 
                                        className={styles.cardImage}
                                        src={event.image} 
                                        width={260} 
                                        height={160} />  
                      </div>
              
                      <div className={cls("glass", styles.col2)}>
                        {event.url && (
                          <div className={styles.iconWrapper}>
                            <Image
                              src="/static/icons/star.svg"
                              width="24"
                              height="24"
                              alt="places icon"
                            />
                            <p className={styles.text}>{event.description}</p>
                          </div>
                        )}
                        
                      </div>
                    </div>
                  </div>
                    ))}
            </div>
        ):(
            <p>No data available</p>
        )}
        </div>
    )
}


export default Event;