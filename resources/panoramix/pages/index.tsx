import Head from "next/head"
import Image from "next/image"
import Banner from '../components/banner'
import Card from '../components/card'
import { Inter } from 'next/font/google'
import styles from "../styles/Home.module.css";

const inter = Inter({ subsets: ['latin'] })

const handleOnBannerBtnClick =() =>{
  console.log("Hi Banner button")
}


export default function Home() {
 

  return (
    <div className={styles.container}>
      <Head>
        <title>Panoramix</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
    <main className={styles.main}>
      <div>
      <Banner buttonText="Buscar Cerca" handleOnClick={handleOnBannerBtnClick}/>
      </div>
      <Image className={styles.heroImage} src="/static/panoramix.png" width={500} height={500}/>
      <Card/>

    </main>
    </div>

  )
}
