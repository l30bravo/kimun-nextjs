import pgsql from  '../../components/db/db'

export default async function handler(req, res) {
  if (req.method === 'POST') {
    return addEvents(req, res)
  } 
  else if (req.method === 'PUT' ||req.method === 'PATCH' ) {
    console.log("under construction")
  }
  else if (req.method === 'DELETE') {
    console.log("under construction")
  }
  else if (req.method === 'GET') {
    return  getEvents(req, res)
  }
  else{

  }
}

//GET
async function getEvents(req, res){
  const { id } = req.query;
  var query = `
    SELECT * FROM event
  `;

  try {

    if (id > 0){
      query = query+ "WHERE id = $1"
      console.log("query: "+query)
      const { rows } = await pgsql.query(query, [id]);
      return res.status(200).json(rows);
    } else{
      console.log("query: "+query)
      const { rows } = await pgsql.query(query);
      return res.status(200).json(rows);
    }

  } catch (error) {
    return res.status(500).json({ error: 'Error fetching data from the database. The error was: '+error });
  }
}


//POST
async function addEvents(req, res){
  console.log("Running addEvents")
  const { image, title, description, url, id_org } = req.body;
  //const values = [req.body]
  var query = `
    INSERT INTO event (image, title, description, url, id_org) VALUES ($1,$2,$3,$4,$5)
  `;

  try {
      const { rows } = await pgsql.query(query, [image, title, description, url, id_org]);
      return res.status(200).json(rows);

  } catch (error) {
    return res.status(500).json({ error: 'Error fetching data from the database. The error was: '+error });
  }
}