import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import StyleFooter from '../styles/Footer.module.css'

export default function App({ Component, pageProps }: AppProps) {
  return (
  <div>
    <Component {...pageProps} />
    <footer>
      <p className={StyleFooter.footer}>© 2023 Panoramix</p>
    </footer>
  </div>

  );
}

