import { Pool } from 'pg';

const pool = new Pool({
  user: process.env.DBUSER,
  host: process.env.DBHOST, // By default, it's usually 'localhost'
  database: process.env.DBNAME,
  password: process.env.DBPASS,
  port: process.env.DBPORT, // Default PostgreSQL port
});

export default pool;