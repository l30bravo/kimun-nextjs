import styles from '../styles/Banner.module.css'

const Banner = (props) => {
    return <div className={styles.container}> 
        <h1 className={styles.title}>
            <span className={styles.title1}>Pano</span>
            <span className={styles.title2}>ramix</span>
        </h1>
        <h3 className={styles.subTitle}>Buscador de Eventos</h3>
        <div className={styles.buttonWrapper}>
        <button className={styles.button} onClick={props.handleOnClick}>
            {props.buttonText}
        </button>
        </div>
        
    </div>;
}


export default Banner;