import Link from "next/link";
import Image from "next/image"
import styles from '../styles/Card.module.css'
import cls from "classnames"
import React, { useEffect, useState } from 'react';
import { Contrail_One } from "next/font/google";

function getStringValue(value: any): string {
    return value + "";
}

const Card = () => {

    const [events, setEvents] = useState([]);
    useEffect(() => {
        // Fetch data from the API endpoint
        fetch('http://local.uchile.cl:3000/api/events')
        .then((response) => response.json())
        .then((data) => setEvents(data))
        .catch((error) => console.error('Error fetching data:', error));
    }, []);

    console.log(events)

    return (
        
        
        <div>
        <h2 className={styles.heading2}>Events</h2>
        <br/><br/>
        {Object.keys(events).length > 0 ? (
                <div className={styles.cardLayout}>
                    {events.map((event) => ( 
                        <div className={styles.cardLink}>
                            <Link href={"event/"+getStringValue(event.id)} >   
                            <div className={cls("glass", styles.container)}>
                                <div className={styles.cardHeaderWrapper}>
                                    <h2 className={styles.cardHeader}>{event.title}</h2>
                                </div>
                                <div className={styles.cardImageWrapper}>
                                    <Image 
                                        className={styles.cardImage}
                                        src={event.image} 
                                        width={260} 
                                        height={160} />   
                                </div>
                            </div>
                            </Link>
                        </div>
                    ))}
                </div>
            ):(
                <p>No data available</p>
             )}
         </div>
         
    )
}

export default Card