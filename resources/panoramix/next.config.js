/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['images.unsplash.com','urbanmilwaukee.com','instagram.fscl13-1.fna.fbcdn.net' ]
  }
}

module.exports = nextConfig
