-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 1.0.2
-- PostgreSQL version: 15.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: panoramix | type: DATABASE --
-- DROP DATABASE IF EXISTS panoramix;
CREATE DATABASE panoramix;
-- ddl-end --


-- object: public.event | type: TABLE --
-- DROP TABLE IF EXISTS public.event CASCADE;
CREATE TABLE public.event (
	id serial NOT NULL,
	image text,
	title text,
	description text,
	url text,
	id_org integer,
	CONSTRAINT event_pk PRIMARY KEY (id)
);
-- ddl-end --
ALTER TABLE public.event OWNER TO postgres;
-- ddl-end --

-- object: public.organization | type: TABLE --
-- DROP TABLE IF EXISTS public.organization CASCADE;
CREATE TABLE public.organization (
	id serial NOT NULL,
	nombre varchar(50),
	url text,
	CONSTRAINT organization_pk PRIMARY KEY (id)
);
-- ddl-end --
ALTER TABLE public.organization OWNER TO postgres;
-- ddl-end --

-- object: fk_org | type: CONSTRAINT --
-- ALTER TABLE public.event DROP CONSTRAINT IF EXISTS fk_org CASCADE;
ALTER TABLE public.event ADD CONSTRAINT fk_org FOREIGN KEY (id_org)
REFERENCES public.organization (id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


