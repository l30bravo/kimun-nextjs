# kimun-nextjs

# ¿What is Nextjs?
Its a framework build on top of React that gives you the flexibility of building scalable apps by allowing you to render content on the server.


# ¿Benefit of Nextjs?
- Diferent rendering techniques (Static site generation (Build time generation), server side rendering (generate pages on the server), incremental site regeneration(generate pages on build in advance server after))
- Performance (image optimization, code splitting, minifying files, pre-fetching assets)
- file base routing
- seo
- serverless functions

# Why is Next.js  Popular?
- It's easier to build and configure apps


# Before starting:
[![TypeScript](/resources/imgs/typescript.png)](https://youtu.be/-xDZwb-PY0M)
[![EsLint](/resources/imgs/eslint.png)](https://youtu.be/QpDpRmlFfqI)
[![TailWind](/resources/imgs/tailwind.png)](https://youtu.be/lURtd-oneaM)

# Middleware

![middleware](resources/imgs/middleware.png)

# Routes
![route](resources/imgs/route.png)

# Next / Link
![middleware](resources/imgs/links.png)

# Pre Rendering
The pre rendering is done in the build stage

* Nextjs
![middleware](resources/imgs/pre-rendering.png)
- quickly load the HTML of the page
- Best performance for the CEO
* React
![middleware](resources/imgs/pre-rendering-react.png)
- It takes longer to load the page
- Low performance for the CEO

## SSG (Static Site Generation): 
HTML is generated at Build time - Render in the server
For example:
* About page
![middleware](resources/imgs/ssg.png)

## ISR (Incremental Site Regeneration)
Rebuild the HTML site after its has been deployed - Render in the server
For example:
* Feed Page like twitter page
* E-Coomerce
![middleware](resources/imgs/isr.png)

## SSR (Server Side Rendering)
HTML is generate for every request - Render in the server
* its definitely a bit slower compared to ISR
* its used when we work with dynamic data
For example
* News Page
![middleware](resources/imgs/ssr.png) 

## CSR (Client Side Rendering)
HTML is generated for everu request - Render in the Client Side (web browser)
Java Script is reponsible for showing it on the page
If you page constains frequently updating data, and you don't need to pre-render the data, you can fetch data on the client side.

For Example:
* Dashboards Pages or user pages where the SEO is not relevant
![middleware](resources/imgs/csr.png)

# CMS
* `npx create-next-app@latest` - create new project
* `npm run dev` - run app
* `npm install` - instalar dependencias / pkgs
* `npm install ${package}@latest` - update package
* `npm audit ` - show dependencies and issues
* `npm audit and fix` - fix packages issues
* `npm audit and fix --force` - fix packages issues
* `npm install classnames@latest` - pkg para combinar styles
* `npm install pg` - instalando pkg para PostgreSQL

# Notes
* `pages/_apps.js` - it's a traversal html code for all pages
* `Fast Refresh` - is a feature that gives you instant feedback on any edits you make in your application, so, any changes will get applied right away without us having to reload the application
* `Routing` - by default is using file system based router. 
# Links

- [https://nextjs.org/docs](https://nextjs.org/docs)
- [npx)(https://docs.npmjs.com/cli/v7/commands/npx) -Run a command from a local or remote npm package
- [npm](https://docs.npmjs.com/about-npm)npm is the world's largest software registry. Open source developers from every continent use npm to share and borrow packages, and many organizations use npm to manage private development as well.
npm consists of three distinct components:
    - the website
    - the Command Line Interface (CLI)
    - the registry
- [Glass generator](https://ui.glass/generator/)

# Router
![router](resources/imgs/1.png)

# SEO
![seo](resources/imgs/2.png)

# Serverless Functions
![serverless-functions](resources/imgs/3.png)
